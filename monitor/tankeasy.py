#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os

def split_user(home, pathname, path, name, root='root'):
    lh=len(home)
    lpn=len(pathname)
    lp=len(path)
    ln=len(name)
    if lh <=0 or ln <=0 or lpn <=0 or lp <=0 or lp <= lh or lpn < ln:
        return "", "", "", False
    # "/var/netdisk"
    # "/var/netdisk/admin/root/?"
    i = pathname.find(home)
    if i < 0:
        return "", "", "", False
    fpath = pathname[1+i+len(home):]
    tmp = pathname[1+i+len(home):]
    i = tmp.find(root)
    if i < 0:
        return "", "", "", False
    user = tmp[:i-1]
    if not user:
        return "", "", "", False
    relative = tmp[i+4:]
    fpath = fpath[i:]
    if not relative:
        return "", "", "", False
    fpath = fpath[len(root):-len(name)-1]
    if not fpath :
        fpath = root
    print(user, relative, fpath)
    return user, relative, fpath, True


if __name__ == '__main__':
    # monitor path
    homepath = "/var/netdisk"
    pathname = "/var/netdisk/admin/root/abc/1"
    path = "/var/netdisk/admin/root/abc"
    name = "1"
    #homepath = "/var/netdisk"
    #pathname = "/var/netdisk/admin/root/abc"
    #path = "/var/netdisk/admin/root/"
    #name = "abc"
    pathname = "/var/netdisk/admin/cache/abc"
    path = "/var/netdisk/admin/cache/"
    name = "abc"
    # manager
    split_user(homepath, pathname, path, name)