#!/usr/bin/python3
# -*- coding: utf-8 -*-
import pyinotify
from tank import *
 
homepath = "/var/netdisk"
db_port = 3306
db_charset = 'utf8'
db_name = 'tank'
db_host = '127.0.0.1'
db_user_name = 'tank'
db_user_pwd = '123456'

tank = Tank(homepath, db_name, db_host, db_user_name, db_user_pwd, db_port, db_charset)
times = 0 
class NetdiskEventHandler(pyinotify.ProcessEvent):   
    def process_IN_CREATE(self, event):
        # create
        print(event)
        global times
        times+=1
        print(times)
        # <Event dir=False mask=0x100 
        # maskname=IN_CREATE name=aaa 
        # path=/var/netdisk/admin/root 
        # pathname=/var/netdisk/admin/root/aaa 
        # wd=5 >
        tank.create(event.dir, event.path, event.name, event.pathname)
        return 
 
    def process_IN_DELETE(self, event):
        # delete
        print(event)
        global times
        times+=1
        print(times)
        # <Event dir=False mask=0x200 
        # maskname=IN_DELETE name=aaa 
        # path=/var/netdisk/admin/root 
        # pathname=/var/netdisk/admin/root/aaa 
        # wd=5 >
        tank.delete(event.dir, event.path, event.name, event.pathname)
        # print("process_IN_DELETE:", event.pathname)
        return 
 
    def process_IN_MOVED_TO(self, event):
        # move to
        print(event)
        global times
        times+=1
        print(times)
        # <Event cookie=80721 dir=False 
        # mask=0x80 maskname=IN_MOVED_TO 
        # name=dd
        # path=/var/netdisk/admin/root 
        # pathname=/var/netdisk/admin/root/dd 
        # src_pathname=/var/netdisk/admin/root/cc
        # wd=5 >
        tank.movedto(event.dir, event.path, event.name, event.pathname, event.src_pathname)
        # print("process_IN_MOVED_TO:", event.pathname)
        return 

    def process_IN_MOVED_FROM(self, event):
        # move from
        print(event)
        global times
        times+=1
        print(times)
        # <Event cookie=80916 dir=True 
        # mask=0x40000040 maskname=IN_MOVED_FROM|IN_ISDIR 
        # name=aaaa 
        # path=/var/netdisk/admin/root 
        # pathname=/var/netdisk/admin/root/aaaa wd=5 >
        tank.moved_from(event.dir, event.path, event.name, event.pathname)
        # print("process_IN_MOVED_FROM:", event.pathname)
        return 

    def process_IN_MOVED(self, event):
        # move from
        print(event)
        global times
        times+=1
        print(times)
        # <Event cookie=80916 dir=True 
        # mask=0x40000040 maskname=IN_MOVED_FROM|IN_ISDIR 
        # name=aaaa 
        # path=/var/netdisk/admin/root 
        # pathname=/var/netdisk/admin/root/aaaa wd=5 >
        # tank.moved(event.dir, event.path, event.name, event.pathname, event.src_pathname)
        # print("process_IN_MOVED_FROM:", event.pathname)
        return 

if __name__ == '__main__':
    # monitor path
    homepath = "/var/netdisk"
    # manager
    watchManager = pyinotify.WatchManager()
    # add homepath to watch
    watchManager.add_watch(homepath, pyinotify.ALL_EVENTS, rec=True, auto_add=True, do_glob=True)
    # create event handler
    handler = NetdiskEventHandler()
    # notifier
    notifier = pyinotify.Notifier(watchManager, handler)
    # run loop
    notifier.loop()